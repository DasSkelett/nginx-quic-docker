image: docker:latest
services:
  - docker:dind

stages:
  - build
  - test
  - deploy

variables:
  DOCKER_BUILDKIT: 1
  CI_IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  CI_IMAGE_ALT_BASE: $CI_REGISTRY_IMAGE/nginx-quic
  CI_BUILD_IMAGE_BASE: $CI_REGISTRY_IMAGE/build
  CI_BUILD_IMAGE_TAG: $CI_REGISTRY_IMAGE/build:$CI_COMMIT_REF_SLUG
  LAST_VERSION_FILE: .last_version
  CI_ENV_FILE: .ci.env

docker-build:
  stage: build
  cache:
    paths:
      - $LAST_VERSION_FILE
  artifacts:
    reports:
      dotenv: $CI_ENV_FILE
    expire_in: 30 minutes
  before_script:
    - apk add bash curl jq
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - chmod +x ./build.sh && /bin/bash ./build.sh
    - docker push "${CI_IMAGE_TAG}"
    - docker push "${CI_BUILD_IMAGE_TAG}"
  rules:
    - if: "$CI_REGISTRY == null"
      when: never
    - changes:
      - "build.sh"
      - "docker-entrypoint.sh"
      - "Dockerfile"
      - "nginx.conf"
      - ".gitlab-ci.yml"
    - if: '$CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "schedule"'


.docker-tests:
  stage: test
  dependencies:
    - docker-build
  services:
    - name: "$CI_IMAGE_TAG"
      alias: nginxquic
  script:
    - curl --http3 -v --silent --insecure "https://nginxquic:443"
  rules:
    - if: "$CI_REGISTRY == null"
      when: never
    - changes:
      - "build.sh"
      - "docker-entrypoint.sh"
      - "Dockerfile"
      - "nginx.conf"
      - ".gitlab-ci.yml"
    - if: '$CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "schedule"'

docker-tests:quiche:
  extends: .docker-tests
  image: ghcr.io/unasuke/curl-http3:quiche-2022-11-22

docker-tests:ngtcp2:
  extends: .docker-tests
  image: ghcr.io/unasuke/curl-http3:ngtcp2-2022-11-22

docker-tests:msh3:
  extends: .docker-tests
  image: ghcr.io/unasuke/curl-http3:msh3-2022-11-22


docker-publish:gl:
  stage: deploy
  dependencies:
    - docker-build
    - docker-tests:quiche
    - docker-tests:ngtcp2
    - docker-tests:msh3
  variables:
    FULL_VERSION: $FULL_VERSION
    VERSION: $VERSION
  cache:
    paths:
      - $LAST_VERSION_FILE
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker pull "${CI_IMAGE_TAG}"

    - docker tag "${CI_IMAGE_TAG}" "${CI_REGISTRY_IMAGE}:latest"
    - docker tag "${CI_IMAGE_TAG}" "${CI_REGISTRY_IMAGE}:${VERSION}"
    - docker tag "${CI_IMAGE_TAG}" "${CI_REGISTRY_IMAGE}:archlinux"
    - docker tag "${CI_IMAGE_TAG}" "${CI_REGISTRY_IMAGE}:${VERSION}-archlinux"
    - docker tag "${CI_IMAGE_TAG}" "${CI_IMAGE_ALT_BASE}:latest"
    - docker tag "${CI_IMAGE_TAG}" "${CI_IMAGE_ALT_BASE}:${VERSION}"
    - docker tag "${CI_IMAGE_TAG}" "${CI_IMAGE_ALT_BASE}:archlinux"
    - docker tag "${CI_IMAGE_TAG}" "${CI_IMAGE_ALT_BASE}:${VERSION}-archlinux"

    - docker push "${CI_REGISTRY_IMAGE}:latest"
    - docker push "${CI_REGISTRY_IMAGE}:${VERSION}"
    - docker push "${CI_REGISTRY_IMAGE}:archlinux"
    - docker push "${CI_REGISTRY_IMAGE}:${VERSION}-archlinux"
    - docker push "${CI_IMAGE_ALT_BASE}:latest"
    - docker push "${CI_IMAGE_ALT_BASE}:${VERSION}"
    - docker push "${CI_IMAGE_ALT_BASE}:archlinux"
    - docker push "${CI_IMAGE_ALT_BASE}:${VERSION}-archlinux"

    - echo $FULL_VERSION > $LAST_VERSION_FILE
  rules:  # Publish only on default branch
    - if: "$CI_REGISTRY == null"
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:  # 'if' and 'changes' are AND-combined
      - "build.sh"
      - "docker-entrypoint.sh"
      - "Dockerfile"
      - "nginx.conf"
      - ".gitlab-ci.yml"
    - if: '$CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "schedule"'

docker-publish:dh:
  stage: deploy
  dependencies:
    - docker-build
    - docker-tests:quiche
    - docker-tests:ngtcp2
    - docker-tests:msh3
  variables:
    VERSION: $VERSION
  before_script:
    - docker login -u "$DP_REGISTRY_USER" -p "$DP_REGISTRY_PASSWORD" $DP_REGISTRY
  script:
    - docker pull "${CI_IMAGE_TAG}"

    - docker tag "${CI_IMAGE_TAG}" "${DP_REGISTRY_IMAGE}:latest"
    - docker tag "${CI_IMAGE_TAG}" "${DP_REGISTRY_IMAGE}:${VERSION}"
    - docker tag "${CI_IMAGE_TAG}" "${DP_REGISTRY_IMAGE}:archlinux"
    - docker tag "${CI_IMAGE_TAG}" "${DP_REGISTRY_IMAGE}:${VERSION}-archlinux"

    - docker push "${DP_REGISTRY_IMAGE}:latest"
    - docker push "${DP_REGISTRY_IMAGE}:${VERSION}"
    - docker push "${DP_REGISTRY_IMAGE}:archlinux"
    - docker push "${DP_REGISTRY_IMAGE}:${VERSION}-archlinux"
  rules:  # Publish only on default branch
    - if: "$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH ||
        $CI_REGISTRY == null || $DP_REGISTRY == null || $DP_REGISTRY_USER == null ||
        $DP_REGISTRY_PASSWORD == null || $DP_REGISTRY_IMAGE == null"
      when: never
    - changes:
      - "build.sh"
      - "docker-entrypoint.sh"
      - "Dockerfile"
      - "nginx.conf"
      - ".gitlab-ci.yml"
    - if: '$CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "schedule"'
