# nginx-quic-docker

This repository holds the Dockerfile and associated files needed to build [registry.gitlab.com/dasskelett/nginx-quic-docker/nginx-quic](https://gitlab.com/DasSkelett/nginx-quic-docker/container_registry/3108222),
an nginx image compiled from the mainline branch with support for HTTP/3 and QUIC.
The image is based on `archlinux:base`.
The Dockerfile is loosely oriented on the [Dockerfile](https://github.com/nginxinc/docker-nginx/blob/master/mainline/alpine/Dockerfile) for the official [nginx:alpine](https://hub.docker.com/_/nginx) image.

Containers will listen for HTTPS (HTTP/1.1 via TLS, HTTP/2 via TLS, HTTP/3 via QUIC) connections on port 443 by default.
The image generates a self-signed certificate at container start for this reason,
at `/etc/nginx/key.pem` (configurable using `$KEY_LOCATION`) and `/etc/nginx/cert.pem` (`$CERT_LOCATION`), _only_ if both these files do not exist yet.
To use RSA instead of ECDSA / P-256, set `$CERT_USE_EC` to `false`.

Keep in mind that HTTP/3+QUIC support in nginx is still WIP.
Check [this guide](https://nginx.org/en/docs/quic.html) and the [HTTP/3 module docs](https://nginx.org/en/docs/http/ngx_http_v3_module.html) for configuration beyond the basic options set in the example config.

[Dockerfile/Source](https://gitlab.com/DasSkelett/nginx-quic-docker/-/blob/master/Dockerfile)

## Usage

Docker command line:
```bash
docker run -d --name nginx-quic -p [::]:80:80 -p [::]:443:443/tcp -p [::]:443:443/udp registry.gitlab.com/dasskelett/nginx-quic-docker/nginx-quic:latest
```

docker-compose.yml:
```yaml
services:
  nginx:
    image: registry.gitlab.com/dasskelett/nginx-quic-docker/nginx-quic:latest
    restart: always
    ports:
      - "80:80"
      - "443:443/tcp"
      - "443:443/udp"
    volumes:
      - /var/www/:/usr/share/nginx/
      - ./nginx/etc/nginx.conf:/etc/nginx/nginx.conf:ro
      - ./nginx/etc/conf.d/:/etc/nginx/conf.d/:ro
      - /etc/letsencrypt/live/domain.tld/fullchain.pem:/etc/nginx/cert.pem:ro
      - /etc/letsencrypt/live/domain.tld/privkey.pem:/etc/nginx/cert.key:ro
```
