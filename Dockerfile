FROM ghcr.io/archlinux/archlinux:base

RUN groupadd --gid 101 --system nginx \
    && useradd --uid 101 --gid nginx --system --create-home --home-dir /var/cache/nginx --shell /sbin/nologin nginx

RUN pacman --noconfirm -Sy \
    && pacman --noconfirm -S nginx-mainline \
    && rm -rf /var/cache/pacman/pkg/*

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

# Generate self-signed HTTPS certificate on startup to demonstrate QUIC capabilities
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
EXPOSE 443/tcp
EXPOSE 443/udp

STOPSIGNAL SIGQUIT

CMD ["nginx", "-g", "daemon off;"]

ARG NGINX_VERSION
ARG PKG_VERSION
ENV NGINX_VERSION=${NGINX_VERSION} PKG_VERSION=${PKG_VERSION}
