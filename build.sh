#!/bin/bash

set -ex

API_RESPONSE=$(curl --silent --fail --location "https://archlinux.org/packages/extra/x86_64/nginx-mainline/json/")
export PKGVER=$(echo -n $API_RESPONSE | jq -r '.pkgver')
export PKGREL=$(echo -n $API_RESPONSE | jq -r '.pkgrel')
export VERSION=$PKGVER
export FULL_VERSION="$PKGVER-$PKGREL"

echo "FULL_VERSION=$FULL_VERSION" > $CI_ENV_FILE && echo "VERSION=$VERSION" >> $CI_ENV_FILE

if [[ -f $LAST_VERSION_FILE ]]; then
  export LAST_VERSION=$(<$LAST_VERSION_FILE)
fi
if [[ -n $LAST_VERSION && $LAST_VERSION != $FULL_VERSION ]]; then
  export CI_USE_CACHE=false
fi
arg_params=(--build-arg NGINX_VERSION="${VERSION}" --build-arg PKG_VERSION="${FULL_VERSION}")
cache_params=()
export arg_params
export cache_params

# https://gitlab.com/gitlab-org/gitlab-foss/-/issues/17861#note_19140733
if [[ $CI_USE_CACHE == true ]]; then
  if docker pull "$CI_BUILD_IMAGE_TAG"; then
    echo "Using existing image $CI_BUILD_IMAGE_TAG as cache source"
    cache_params+=(--cache-from "$CI_BUILD_IMAGE_TAG")
  elif docker pull "$CI_BUILD_IMAGE_BASE:main"; then
    echo "Falling back to $CI_BUILD_IMAGE_BASE:main as cache source"
    cache_params+=(--cache-from "$CI_BUILD_IMAGE_BASE:main")
  else
    echo "No available cache source found, building fresh"
    cache_params+=(--no-cache)
  fi
else
  echo "CI_USE_CACHE is false"
  cache_params+=(--no-cache)
fi

docker build --progress=plain "${arg_params[@]}" "${cache_params[@]}" --build-arg BUILDKIT_INLINE_CACHE=1 -t "${CI_BUILD_IMAGE_TAG}" .
docker build --progress=plain "${arg_params[@]}" --cache-from "${CI_BUILD_IMAGE_TAG}" -t "${CI_IMAGE_TAG}" .
